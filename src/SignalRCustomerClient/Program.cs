﻿using System.Text.Json;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

var connection = new HubConnectionBuilder()
    .WithUrl("https://localhost:5001/hubs/customer")
    .Build();


await connection.StartAsync();

var result1 = await connection.InvokeCoreAsync("GetCustomers", typeof(List<CustomerShortResponse>), new object[]{});
Console.WriteLine("GetCustomers");
Console.WriteLine(JsonSerializer.Serialize(result1));

Console.WriteLine("__________________________________________________________");

var param = new object[] { Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0") };
var result2 = await connection.InvokeCoreAsync("GetCustomer", typeof(CustomerResponse), param);
Console.WriteLine("GetCustomer");
Console.WriteLine(JsonSerializer.Serialize(result2));

Console.WriteLine("__________________________________________________________");

var result3 = await connection.InvokeCoreAsync("DeleteCustomer", typeof(CustomerResponse), param);
Console.WriteLine("DeleteCustomer");
Console.WriteLine(JsonSerializer.Serialize(result3));

await connection.StopAsync();

Console.WriteLine("Нажмите любую клавишу");
Console.ReadKey();

