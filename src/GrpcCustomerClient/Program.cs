﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcCustomer;

// создаем канал для обмена сообщениями с сервером
// параметр - адрес сервера gRPC
using var channel = GrpcChannel.ForAddress("https://localhost:5001");
// создаем клиент
var client = new grpcCustomer.grpcCustomerClient(channel);
var parEm = new Google.Protobuf.WellKnownTypes.Empty();
const string parId = "a6c8c6b1-4349-45b0-ab31-244740aaf0f0";
const string parIdPref = "c4bda62e-fc74-4256-a956-4760b3858cbd";
var idReq = new IdRequestGrpc() { Id = parId };

try
{
    var customers = client.GetCustomers(parEm);
    Console.WriteLine("Метод GetCustomers");
    Console.WriteLine(customers);
}
catch (RpcException ex)
{
    Console.WriteLine(ex.Status.Detail); // получаем статус ответа
}


try
{
    var customerGet = client.GetCustomer(idReq);
    Console.WriteLine("Метод GetCustomer");
    Console.WriteLine(customerGet);
}
catch (RpcException ex)
{
    Console.WriteLine(ex.Status.Detail); // получаем статус ответа
}


try
{
    var testCr = new CreateCustomerRequest()
    {
        Email = "ivanov@mail.ru",
        FirstName = "Cемен",
        LastName = "Иванов"
    };
    testCr.PreferenceIds.Add(parIdPref);

    var customerCr = client.CreateCustomer(testCr);
    Console.WriteLine("Метод CreateCustomer");
    Console.WriteLine(customerCr);
}
catch (RpcException ex)
{
    Console.WriteLine(ex.Status.Detail); // получаем статус ответа
}


try
{
    var testEd = new EditCustomerRequest()
    {
        Id = parId,
        Email = "ivanov@mail.ru",
        FirstName = "Cемен",
        LastName = "Иванов"
    };
    testEd.PreferenceIds.Add(parIdPref);

    var customerEd = client.EditCustomer(testEd);
    Console.WriteLine("Метод EditCustomer");
    Console.WriteLine(customerEd);
    var customerGet = client.GetCustomer(idReq);
    Console.WriteLine("Проверка метода EditCustomer");
    Console.WriteLine(customerGet);
}
catch (RpcException ex)
{
    Console.WriteLine(ex.Status.Detail); // получаем статус ответа
}

try
{
    var customerDel = client.DeleteCustomer(idReq);
    Console.WriteLine("Метод DeleteCustomer");
    Console.WriteLine(customerDel);
    Console.WriteLine("Проверка метода DeleteCustomer");
    var customerGet = client.GetCustomer(idReq);
    Console.WriteLine("Проверка метода DeleteCustomer");
    Console.WriteLine(customerGet);
}
catch (RpcException ex)
{
    Console.WriteLine(ex.Status.Detail); // получаем статус ответа
}

Console.ReadKey();