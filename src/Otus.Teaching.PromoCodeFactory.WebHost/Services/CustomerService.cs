﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System.Linq;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.GrpcCustomer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class GrpcCustomerService : GrpcCustomer.grpcCustomer.grpcCustomerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public GrpcCustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<ListCustomerShortResponceGrpc> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new ListCustomerShortResponceGrpc();
            response.CustomersShort.AddRange(customers.Select(x => new CustomerShortGrpc()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }));

            return response;
        }

        public override async Task<CustomerResponceGrpc> GetCustomer(IdRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));

            var response = CustomerMapper.MapFromModel(customer);

            return response;
        }

        public override async Task<IdRequestGrpc> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferenceIds = request.PreferenceIds.Select(x => Guid.Parse(x)).ToList();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new IdRequestGrpc() { Id= customer.Id.ToString() };
        }
        public override async Task<Empty> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null) 
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));

            var preferenceIds = request.PreferenceIds.Select(x => Guid.Parse(x)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(IdRequestGrpc request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, "User not found"));

            await _customerRepository.DeleteAsync(customer);
            return new Empty();
        }
    }
}
